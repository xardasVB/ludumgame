﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sacrifice : MonoBehaviour
{
    public float speed = 3f;
    public float counter = 0;

    Rigidbody2D rigidbody;

    private void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        counter += Time.deltaTime;
        if (counter > 10)
            Destroy(gameObject);

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            var player = collision.gameObject.GetComponent<Player>();
            if (player.sacrificeCount < player.maxSacrificeCount)
            {
                player.lights[player.sacrificeCount].SetActive(true);
                player.sacrificeCount++;
                player.collected++;
                Destroy(gameObject);
            }
        }
    }
}
