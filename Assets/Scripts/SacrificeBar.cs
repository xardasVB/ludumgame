﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SacrificeBar : MonoBehaviour
{
    public Player player;
    Image foregroundImage;

    public float Value
    {
        get
        {
            if (foregroundImage != null)
                return (int)(foregroundImage.fillAmount * 100);
            else
                return 0;
        }
        set
        {
            if (foregroundImage != null)
                foregroundImage.fillAmount = value / 100f;
        }
    }

    void Start()
    {
        foregroundImage = gameObject.GetComponent<Image>();
    }

    private void Update()
    {
        Value = (player.sacrificeCount * 100) / player.maxSacrificeCount;
    }
}
