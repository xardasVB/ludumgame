﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour {

    public float speed = 2f;
    public float startpoint = 36f;
    public float endpoint = -18f;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.Translate(Vector3.left * speed * Time.deltaTime);

        if (transform.position.x < endpoint)
        {
            transform.Translate(Vector3.right * startpoint);
        }
	}
}
