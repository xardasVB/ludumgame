﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{

    public float minPos = -2.5f;
    public float maxPos = 5.5f;
    public float minSec = 0.5f;
    public float maxSec = 5;
    public float minSpawn = 1;
    public float maxSpawn = 2;
    public float minSpeed = 4;
    public float maxSpeed = 8;
    public List<GameObject> objects;

    float counter = 0;
    float range = 2;
    int randomNum = 0;

    public virtual void UpdatePositions() { }

    void Update()
    {
        UpdatePositions();
        counter += Time.deltaTime;

        if (counter >= range)
        {
            range = Random.Range(minSec, maxSec);
            counter = 0;
            for (int i = 0; i < Random.Range(minSpawn, maxSpawn); i++)
            {
                randomNum = Random.Range(0, randomNum == 9 ? objects.Count - 1 : objects.Count);
                Spawn(objects[randomNum]);
            }
        }
    }

    void Spawn(GameObject obj)
    {
        GameObject en = Instantiate(obj, new Vector3(transform.position.x, Random.Range(minPos, maxPos), 0), transform.rotation);
        en.GetComponent<Enemy>().speed = Random.Range(minSpeed, maxSpeed);
        if (en.tag == "kaktus") en.transform.Rotate(0, 0, 90);
    }
}
