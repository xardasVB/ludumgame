﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {
    
    public float score = 0f;
    public int collected = 0;
    public int sacrificesMade = 0;

    public int maxSacrificeCount = 9;
    public int sacrificeCount = 9;
    public float speed = 5f;
    public float force = 200f;
    float realForce;
    public float gravity = 1f;
    public float manuverability = 200f;
    public float drag = 0.2f;
    public float deltaGravity = 0.5f;
    public float maxGravity = 4f;
    public GameObject sacrifice;
    public List<GameObject> lights;
    public List<GameObject> damageSprites;
    public List<GameObject> redSprites;
    int currentRedSprite = 0;
    int currentSprite = 0;
    bool changeSprite = true;

    public float steeringDelayTime = 1f;
    float remainingDelayTime;
    public bool steeringDelay = false;
    public bool damageflash = true;
    int flipSide = 1;
    float delta = 1.5f;

    bool gameOver = false;

    Rigidbody2D rigidbody;
    
    void Start () {
        realForce = force;
        rigidbody = GetComponent<Rigidbody2D>();
        remainingDelayTime = steeringDelayTime;
    }
    
    void Update() {
        if (gameOver)
        {
            redSprites[currentRedSprite].SetActive(true);
            gravity += Time.deltaTime;
            rigidbody.AddForce(Vector2.down * gravity);
            return;
        }

        score += Time.deltaTime * 8;
        rigidbody.AddForce(Vector2.down * gravity);

        if (rigidbody.velocity.y >= -1)
            rigidbody.velocity = new Vector2(rigidbody.velocity.x, rigidbody.velocity.y - 0.05f);
        else
            rigidbody.velocity = new Vector2(rigidbody.velocity.x, -1);

        if (rigidbody.velocity.x > 0)
        {
            if (steeringDelay == false)
            {
                transform.rotation = Quaternion.Euler(0, 0, rigidbody.velocity.x * (-2));
                rigidbody.rotation = rigidbody.velocity.x * (-1);
            }
            rigidbody.velocity = new Vector2(rigidbody.velocity.x - drag, rigidbody.velocity.y);
        }
        else
        {
            if (steeringDelay == false)
            {
                transform.rotation = Quaternion.Euler(0, 0, rigidbody.velocity.x * (-2));
                rigidbody.rotation = rigidbody.velocity.x * (-1);
            }
            rigidbody.velocity = new Vector2(rigidbody.velocity.x + drag, rigidbody.velocity.y);
        }

        if (Input.GetKeyDown(KeyCode.Space) && sacrificeCount > 0 && steeringDelay == false)
        {
            rigidbody.AddForce(Vector2.up * force);
            GameObject sac = Instantiate(sacrifice, transform.position, transform.rotation);
            sac.GetComponent<Rigidbody2D>().AddForce(Vector2.left * Random.Range(-100, 100));
            //sac.GetComponent<Rigidbody2D>().AddForce(Vector2.down * 1000);
            sac.GetComponent<Rigidbody2D>().angularVelocity = Random.Range(-180, 180);
            sacrificeCount--;
            lights[sacrificeCount].SetActive(false);
            sacrificesMade++;
        }
        if (Input.GetKey(KeyCode.LeftArrow) && steeringDelay == false)
        {
            if (Mathf.Abs(rigidbody.velocity.x) < speed)
                rigidbody.AddForce(Vector2.left * manuverability);
        }
        if (Input.GetKey(KeyCode.RightArrow) && steeringDelay == false)
        {
            if (Mathf.Abs(rigidbody.velocity.x) < speed)
                rigidbody.AddForce(Vector2.right * manuverability);
        }
        if (steeringDelay) BackFlip();
    }

    public void Hit()
    {
        if (currentSprite == damageSprites.Count - 1) gameOver = true;
        changeSprite = !changeSprite;
        if (currentSprite < damageSprites.Count - 1 && changeSprite)
        {
            damageSprites[currentSprite].SetActive(false);
            currentSprite++;
            redSprites[currentRedSprite].SetActive(false);
            currentRedSprite++;
            damageSprites[currentSprite].SetActive(true);
        }

        realForce += force * 0.25f;
        rigidbody.AddForce(Vector2.up * force / 1.5f);
        steeringDelay = true;
        int random = Random.Range(0, 2);
        if (random == 0) flipSide = 1;
        else flipSide = -1;
        delta = 2.5f;
    }

    public void BackFlip()
    {

        delta = (Mathf.Pow((2 * (remainingDelayTime - steeringDelayTime / 3)) / steeringDelayTime, 3)) * 2.5f;
        transform.Rotate(0, 0, Time.deltaTime / steeringDelayTime * 360 * flipSide * delta);
        rigidbody.MoveRotation(Time.deltaTime / steeringDelayTime * 360 * flipSide * (-1) * delta);
        remainingDelayTime -= Time.deltaTime;
        if (damageflash && remainingDelayTime > steeringDelayTime / 1.33) redSprites[currentRedSprite].SetActive(true);
        else redSprites[currentRedSprite].SetActive(false);

        if (remainingDelayTime < 0)
        {
            steeringDelay = false;
            remainingDelayTime = steeringDelayTime;
            transform.rotation = Quaternion.Euler(0, 0, 0);
            rigidbody.rotation = 0;
            if (gravity + deltaGravity < maxGravity)
                gravity += deltaGravity;
        }
    }
}

