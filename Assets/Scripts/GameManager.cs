﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour {
    public float startingMinSec = 2;
    public float endingMinSec = 0.1f;
    public float startingMaxSec = 6;
    public float endingMaxSec = 1;
    public float startingMinSpawn = 1;
    public float endingMinSpawn = 3;
    public float startingMaxSpawn = 1;
    public float endingMaxSpawn = 3;
    public float startingMinSpeed = 1;
    public float endingMinSpeed = 10;
    public float startingMaxSpeed = 5;
    public float endingMaxSpeed = 10;
    public float startingSacMaxSec = 6;
    public float endingSacMaxSec = 3;
    public float startingSacMaxSpawn = 1;
    public float endingSacMaxSpawn = 10;
    public EnemySpawner enemySpawner;
    public List<SacrificeSpawner> sacrificeSpawner;

    public float timeToMaxDifficulty = 120;

    float deltaMinSec, deltaMaxSec, deltaMinSpawn, deltaMaxSpawn, deltaMinSpeed, deltaMaxSpeed;
    float deltaSacMaxSec, deltaSacMaxSpawn;

    float counter = 0;

    float currentMinSpawn, currentMaxSpawn, currentSacMaxSpawn;

	void Start () {
        deltaMinSec = (startingMinSec - endingMinSec) / timeToMaxDifficulty * 2;
        deltaMaxSec = (startingMaxSec - endingMaxSec) / timeToMaxDifficulty * 2;
        deltaMinSpawn = (startingMinSpawn - endingMinSpawn) / timeToMaxDifficulty * 2;
        deltaMaxSpawn = (startingMaxSpawn - endingMaxSpawn) / timeToMaxDifficulty * 2;
        deltaMinSpeed = (startingMinSpeed - endingMinSpeed) / timeToMaxDifficulty;
        deltaMaxSpeed = (startingMaxSpeed - endingMaxSpeed) / timeToMaxDifficulty;
        deltaSacMaxSec = (startingSacMaxSec - endingSacMaxSec) / timeToMaxDifficulty * 3;
        deltaSacMaxSpawn = (startingSacMaxSpawn - endingSacMaxSpawn) / timeToMaxDifficulty * 2;

        enemySpawner.minSec = startingMinSec;
        enemySpawner.maxSec = startingMaxSec;
        enemySpawner.minSpawn = startingMinSpawn;
        enemySpawner.maxSpawn = startingMaxSpawn;
        enemySpawner.minSpeed = startingMinSpeed;
        enemySpawner.maxSpeed = startingMaxSpeed;
        for (int i = 0; i < sacrificeSpawner.Count; i++)
        {
            sacrificeSpawner[i].minSec = 2;
            sacrificeSpawner[i].maxSec = startingSacMaxSec;
            sacrificeSpawner[i].minSpawn = 1;
            sacrificeSpawner[i].maxSpawn = sacrificeSpawner[i].chances.Count;
        }

        currentMaxSpawn = startingMaxSpawn;
        currentMinSpawn = startingMinSpawn;
        currentSacMaxSpawn = startingSacMaxSpawn;
    }
	
    public void BecomeHarder()
    {
        if (enemySpawner.minSec > endingMinSec) enemySpawner.minSec -= deltaMinSec;
        if (enemySpawner.maxSec > endingMaxSec && enemySpawner.minSec <= endingMinSec) enemySpawner.maxSec -= deltaMaxSec;
        if (enemySpawner.minSpawn < endingMinSpawn && currentMaxSpawn >= endingMaxSpawn)
        {
            currentMinSpawn = currentMinSpawn - deltaMinSpawn;
            enemySpawner.minSpawn = Mathf.FloorToInt(currentMinSpawn);
        }
        if(enemySpawner.maxSpawn < endingMaxSpawn)
        {
            currentMaxSpawn -= deltaMaxSpawn;
            enemySpawner.maxSpawn  = Mathf.FloorToInt(currentMaxSpawn);
        }
        if (enemySpawner.minSpeed < endingMinSpeed) enemySpawner.minSpeed -= deltaMinSpeed;
        if (enemySpawner.maxSpeed < endingMaxSpeed) enemySpawner.maxSpeed -= deltaMaxSpeed;

            if (sacrificeSpawner[0].maxSpawn < endingSacMaxSpawn)
            {
                currentSacMaxSpawn -= deltaSacMaxSpawn;
                for (int i = 0; i < sacrificeSpawner.Count; i++) sacrificeSpawner[i].maxSpawn = Mathf.FloorToInt(currentSacMaxSpawn);
            }

            if (sacrificeSpawner[0].maxSec > endingSacMaxSec) for (int i = 0; i < sacrificeSpawner.Count; i++) sacrificeSpawner[i].maxSec -= deltaSacMaxSec;
    }

	void Update () {
        counter += Time.deltaTime;
        if(counter >= 1)
        {
            counter = 0;
            BecomeHarder();
        }
	}
}
