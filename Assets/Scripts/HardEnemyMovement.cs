﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HardEnemyMovement : MonoBehaviour {

    public GameObject hardEnemy;
    public GameObject target1, target2;
    public float maxSpeedMultiplier = 4;
    public float targetChangeDistance = 0.1f;

    GameObject target, previousTarget;
    float maxSpeed = 4;
    float speed;

    void UpdateSpeed()
    {
        float distanceToTarget1 = (target1.transform.position - transform.position).magnitude;
        float distanceToTarget2 = (target2.transform.position - transform.position).magnitude;
        float fullDistance = (target1.transform.position - target2.transform.position).magnitude;
        speed = (distanceToTarget1 / fullDistance) * (distanceToTarget2 / fullDistance) * maxSpeed;
    }

	void Start () {
        target = target1;
        maxSpeed = hardEnemy.GetComponent<Enemy>().speed * maxSpeedMultiplier;
	}
	
	void Update () {

        if ((target.transform.position - transform.position).magnitude <= targetChangeDistance)
        {
            if (target == target1)
            {
                target = target2;
                previousTarget = target1;
            }
            else
            {
                target = target1;
                previousTarget = target2;
            }
        }
        UpdateSpeed();
        transform.Translate((target.transform.position - transform.position) / (target.transform.position - transform.position).magnitude * speed * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.gameObject.GetComponent<Player>().Hit();
            Destroy(gameObject);
        }
    }
}
