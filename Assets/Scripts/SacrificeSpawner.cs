﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SacrificeSpawner : MonoBehaviour
{

    public float minPos = -7.5f;
    public float maxPos = 8f;
    public float minSec = 1;
    public float maxSec = 6;
    public int minSpawn = 1;
    public int maxSpawn = 1;
    public List<GameObject> objects;
    public List<int> chances;

    float counter = 0;
    float range = 2;

    int random = 0;

    void Update()
    {
        counter += Time.deltaTime;

        if (counter >= range)
        {
            range = Random.Range(minSec, maxSec);
            counter = 0;
            random = Random.Range(0, maxSpawn);
            for (int i = minSpawn; i <= chances[random]; i++)
            {
                GameObject obj = Instantiate(objects[Random.Range(0, objects.Count)], new Vector3(transform.position.x + Random.Range(minPos, maxPos), transform.position.y, 0), transform.rotation);
                obj.GetComponent<Sacrifice>().speed = Random.Range(2, 4);
                obj.GetComponent<Rigidbody2D>().angularVelocity = Random.Range(-30, 31);
                obj.GetComponent<Rigidbody2D>().drag = Random.Range(2, 10);

            }
        }
    }
}
