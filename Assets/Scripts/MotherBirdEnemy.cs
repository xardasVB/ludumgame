﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotherBirdEnemy : Enemy {
    public float realSpeed = 2f;
    MotherBirdSpawner mbs;

    private void Start()
    {
        mbs = GetComponentInChildren<MotherBirdSpawner>();
        speed = realSpeed;
        speed *= speedMultiplier;
    }
}
