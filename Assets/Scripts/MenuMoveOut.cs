﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuMoveOut : MonoBehaviour {
    public Vector3 startingPoint;
    public Vector3 targetPoint;
    public float speedMultiplier;
    public bool start = false;
	
	void Update () {
		if(start)
        {
            transform.Translate(new Vector3(-1, 0, 0));
            transform.localPosition = Vector3.MoveTowards(transform.localPosition, targetPoint, (startingPoint - transform.localPosition).magnitude * speedMultiplier * Time.deltaTime);
            if ((startingPoint - transform.localPosition).magnitude >= 2000)
            {
                GetComponentInParent<UIGen>().ButtonPressed();
            }
            
        }
	}

    public void UpdateStartingPoint()
    {
        startingPoint = transform.position;
    }
}
