﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuMove : MonoBehaviour
{
    public Vector2 targetPoint;
    public float speedMultiplier = 2;
    public bool move = true;

    void Start()
    {
    }


    void Update()
    {

        if (move)
        {
            if (Vector2.Distance(transform.localPosition, targetPoint) > 3) transform.localPosition = Vector2.MoveTowards(transform.localPosition, targetPoint, Vector2.Distance(transform.localPosition, targetPoint) * Time.deltaTime * speedMultiplier);
            else
            {
                transform.localPosition = targetPoint;
                move = false;
            }
        }

    }
}
