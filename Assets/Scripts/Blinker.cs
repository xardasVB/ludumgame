﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blinker : MonoBehaviour {
    public Player player;
    public GameObject redAlarm;
    public List<GameObject> lights;
    bool active;
    public bool alarm;
    public float frquency = 1;
    float counter = 0;

	void Start () {
        lights = new List<GameObject>();
        for (int i = 0; i < 3; i++) lights.Add(player.lights[i]);
	}
	
	void Update () {
        if (player.sacrificeCount <= 3) alarm = true;
        else alarm = false;

        if(alarm)
        {
            if (counter > 0.7f)
            {
                if (lights[0].GetComponent<SpriteRenderer>().color == Color.red) for (int i = 0; i < 3; i++) lights[i].GetComponent<SpriteRenderer>().color = Color.white;
                else for (int i = 0; i < 3; i++) lights[i].GetComponent<SpriteRenderer>().color = Color.red;

                active = !active;
                redAlarm.SetActive(active);
                counter = 0;
            }
            else counter += Time.deltaTime;
        }
        else
        {
            counter = 0;
            redAlarm.SetActive(false);
            for (int i = 0; i < 3; i++) lights[i].GetComponent<SpriteRenderer>().color = Color.white;
        }
	}
}
