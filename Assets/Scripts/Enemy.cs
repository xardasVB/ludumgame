﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    public float speedMultiplier = 1;
    public float speed = 5f;
    public float counter = 0;
    public float lifetime = 20;
    public Vector2 moveDirection;

    private void Start()
    {
        speed *= speedMultiplier;
    }

    void Update ()
    {
        counter += Time.deltaTime;
        if (counter > lifetime)
            Destroy(gameObject);
        transform.Translate(moveDirection * speed * Time.deltaTime);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.gameObject.GetComponent<Player>().Hit();
            Destroy(gameObject);
        }
    }
}
