﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainUI : UIGen {

    public Player player;
    public Text scoreCount;
    public GameObject restartBoard;
    public Text score;
    public Text collected;
    public Text sacrifices;
    public Button restartBtn;
    public Button backToMMBtn;
    public GameObject sacrificeCounter, scoreCounter;

    Button pressed;

    private void Start()
    {
        restartBtn.onClick.AddListener(Restart);
        backToMMBtn.onClick.AddListener(ToMainMenu);
    }

    void Update ()
    {
        //if (Input.GetKeyDown(KeyCode.Space) && restartBoard.activeSelf)
            //Restart();

        if (player == null)
        {
            restartBoard.SetActive(true);
            sacrificeCounter.SetActive(false);
            scoreCounter.SetActive(false);
            
        }

        scoreCount.text = ((int)Convert.ToDouble(player.score.ToString())).ToString();
        score.text = scoreCount.text;
        collected.text = player.collected.ToString();
        sacrifices.text = player.sacrificesMade.ToString();
    }

    void Restart()
    {
        pressed = restartBtn;
        GetComponentInChildren<MenuMoveOut>().start = true;
        GetComponentInChildren<MenuMove>().move = false;
    }

    void ToMainMenu()
    {
        pressed = backToMMBtn;
        GetComponentInChildren<MenuMoveOut>().start = true;
        GetComponentInChildren<MenuMove>().move = false;
    }

    public override void ButtonPressed()
    {
        if (pressed == backToMMBtn) SceneManager.LoadScene("MainMenu");
        else SceneManager.LoadScene("Level1");
    }
}
