﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuUI : UIGen {
    public Button newGame;
    public Button exit;

    public GameObject mainMenuPanel;
    public GameObject settingsPanel;

    Button pressed;


    void Start ()
    {
        newGame.onClick.AddListener(NewGame);
        exit.onClick.AddListener(Exit);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void NewGame()
    {
        pressed = newGame;
        mainMenuPanel.GetComponent<MenuMoveOut>().UpdateStartingPoint();
        mainMenuPanel.GetComponent<MenuMoveOut>().start = true;
        mainMenuPanel.GetComponent<MenuMove>().move = false;
    }


    void Exit()
    {
        pressed = exit;
        mainMenuPanel.GetComponent<MenuMoveOut>().UpdateStartingPoint();
        mainMenuPanel.GetComponent<MenuMoveOut>().start = true;
        mainMenuPanel.GetComponent<MenuMove>().move = false;

    }

    public override void ButtonPressed()
    {
        if (pressed == newGame) SceneManager.LoadScene("Level1");
        else Application.Quit();
    }
}
